Rails.application.routes.draw do
  get 'static/home'
  resources :products

  root to: "static#home"
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
